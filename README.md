# Segmentation

## Commandes

### Entraînement d'un modèle

    python segmentation.py train train_file model_file

* *train_file* : le fichier d'entraînement, où les limites de phrase sont marquées par "{S}".
* *model_file* : le fichier du modèle (json).

### Segmentation d'un texte

    python segmentation.py test test_file model_file output_file1 output_file2

* *test_file* : le texte brut à segmenter.
* *output_file1* : fichier de sortie où les limites de phrase sont marquées par "{S}" (inséré dans la mise en forme originale du texte).
* *output_file2* : fichier de sortie où chaque phrase est disposée sur une ligne.

### Evaluation de la segmentation

    python segmentation.py eval gold_file system_file

* *gold_file* : le fichier avec la segmentation de référence (les limites de phrase sont marquées par "{S}").
* *system_file* : le fichier qui contient la segmentation à évaluer.

### Format HTML avec balises <span>

    python segmentation.py html input_file output_file

* *input_file* : fichier d'entrée où les limites de phrase sont marquées par "{S}".
* *output_file* : fichier de sortie où les limites sont marquées par des balises <span>.

### Exemples

    python segmentation.py train corpus/train.snt model.json
    python segmentation.py test corpus/dev.txt model.json preds.snt preds.txt
    python segmentation.py eval corpus/dev.snt preds.snt
    python segmentation.py html preds.snt preds.html