import sys
import re
import json

boundaryTag = r'{S}'

upperCase = re.compile(r'[A-Z]')
lowerCase = re.compile(r'[a-z]')
digit = re.compile(r'[0-9]')
boundary = re.compile(boundaryTag)
marker = re.compile(r'[.!?\n]')
markedToken = re.compile(r'\S*[.!?]\S*')

def convertToClass(s):
    s, _ = upperCase.subn(r'A', s)
    s, _ = lowerCase.subn(r'a', s)
    s, _ = digit.subn(r'0', s)
    return s

def localToken(tokens, index):
    for tok in tokens:
        if tok.start() <= index < tok.end():
            return tok.group()
    return ''

def train(trainFileName, modelFileName, nleft, nright):
    classesFrequency = dict()
    misleadingTokens = dict()
    
    trainFile = open(trainFileName, 'r')
    
    line = trainFile.readline()
    nextLine = trainFile.readline()
    
    while len(line) > 2:
        # Recueil des classes de motifs de caracteres entourant les limites de phrase
        for bd in boundary.finditer(line):
            # Le motif est constitue d'une partie a gauche de la limite, et d'une partie a droite
            patternStart, patternEnd = bd.start() - nleft, bd.end() + nright
            
            leftPattern = line[patternStart : bd.start()]
            if patternEnd > len(line):
                rightPattern = line[bd.end() :] + nextLine[: patternEnd - len(line)]
            else:
                rightPattern = line[bd.end() : patternEnd]
                
            pattern = leftPattern + rightPattern
            patternClass = convertToClass(pattern)
            
            if patternClass in classesFrequency:
                classesFrequency[patternClass] += 1
            else:
                classesFrequency[patternClass] = 1
        
        # Recueil des tokens contenant une ponctuation qui ne correspond pas a une limite de phrase (abreviation par ex...)
        for tok in markedToken.findall(line):
            if not tok.endswith(boundaryTag):
                if tok in misleadingTokens:
                    misleadingTokens[tok] += 1
                else:
                    misleadingTokens[tok] = 1
        
        line = nextLine
        nextLine = trainFile.readline()
    
    trainFile.close()
    
    with open(modelFileName, 'w') as modelFile:
        model = [nleft, nright, classesFrequency, misleadingTokens]
        json.dump(model, modelFile, ensure_ascii=False, indent=3, sort_keys=True)


def test(testFileName, modelFileName, sntOutputFileName, txtOutputFileName, threshold):
    with open(modelFileName, 'r') as modelFile:
        nleft, nright, classesFrequency, misleadingTokens = json.load(modelFile)
    
    testFile = open(testFileName, 'r')
    sntOutputFile = open(sntOutputFileName, 'w')
    txtOutputFile = open(txtOutputFileName, 'w')
    
    line = testFile.readline()
    nextLine = testFile.readline()
    
    while len(line) > 2:
        markedTokens = markedToken.finditer(line)
        previousBoundary = 0
        sntOutputLine = ''
        
        # On repere les marques de ponctuation, avant d'effectuer la detection de limite de phrase
        for mk in marker.finditer(line):
            score = 0
            boundary = mk.end()
            patternStart, patternEnd = mk.start() - nleft + 1, mk.end() + nright
            
            # Pour gerer le cas de la ponctuation a l'interieur de guillemets, on decale le motif
            if (mk.end() < len(line)) and (line[mk.end()] == '\"'):
                patternStart += 1
                patternEnd += 1
                boundary += 1
            
            if patternEnd > len(line):
                pattern = line[patternStart :] + nextLine[: patternEnd - len(line)]
            else:
                pattern = line[patternStart : patternEnd]
            patternClass = convertToClass(pattern)
            # On utilise un indice de probabilite - score - fonde d'une part sur la frequence de la classe de motif entourant la ponctuation...
            if patternClass in classesFrequency:
                score += classesFrequency[patternClass]# a ponderer...
            
            token = localToken(markedTokens, mk.start())
            # ...et d'autre part sur la frequence du token dans des cas ambigus (ponctuation mais pas de limite de phrase)
            if token in misleadingTokens:
                score -= misleadingTokens[token]# a ponderer...
                #Debug# print(token, score)
            
            # Detection par comparaison de l'indice a un seuil
            if score > threshold:
                sntOutputLine += line[previousBoundary : boundary] + boundaryTag
                txtOutputFile.write(line[previousBoundary : boundary].strip() + '\n')
                previousBoundary = boundary
        
        sntOutputLine += line[previousBoundary :]
        sntOutputFile.write(sntOutputLine)
        line = nextLine
        nextLine = testFile.readline()
    
    testFile.close()
    sntOutputFile.close()
    txtOutputFile.close()

def eval(goldFileName, systemFileName):
    n = len(boundaryTag)
    truePos, falsePos, falseNeg = 0, 0, 0
    
    goldFile = open(goldFileName, 'r')
    systemFile = open(systemFileName, 'r')
    
    for goldLine, sysLine in zip(goldFile, systemFile):
        goldBoundaries = frozenset([bd.start() - n*i for i, bd in enumerate(boundary.finditer(goldLine))])
        sysBoundaries = frozenset([bd.start() - n*i for i, bd in enumerate(boundary.finditer(sysLine))])
        
        TP = len(goldBoundaries.intersection(sysBoundaries))
        FP = len(sysBoundaries) - TP
        FN = len(goldBoundaries) - TP
        
        truePos += TP
        falsePos += FP
        falseNeg += FN
    
    goldFile.close()
    systemFile.close()
    
    precision = truePos/(truePos + falsePos)
    recall = truePos/(truePos + falseNeg)
    print("Results:\n  True Positives: %d\n  False Positives: %d\n  False Negatives: %d\n" % (truePos, falsePos, falseNeg))
    print("  Precision = %.2f\n  Recall = %.2f\n" % (precision, recall))

def formatToHtml(inputFileName, outputFileName):
    inputFile = open(inputFileName, 'r')
    outputFile = open(outputFileName, 'w')
    
    spanAtBeginning = True
    for line in inputFile:
        line = line.rstrip().replace(boundaryTag, "</span><span>")
        if spanAtBeginning:
            line = "<span>" + line
        if line.endswith("<span>"):
            line = line[:-6]
            spanAtBeginning = True
        else:
            spanAtBeginning = False
        outputFile.write(line + '\n')
    
    if not line.endswith("</span>"):
        outputFile.write("</span>")
    
    inputFile.close()
    outputFile.close()

# ---------------------------------------------------------------------------- #

if __name__ == "__main__":
    usage = '''Usage:
    %s train train_file(inserted boundary marks) model_file
    %s test test_file(raw text) model_file output_file1(inserted boundary marks) output_file2(one sentence per line)
    %s eval gold_file(inserted boundary marks) system_file(inserted boundary marks)
    %s html input_file(inserted boundary marks) output_file(inserted <span> tag)'''
    if len(sys.argv) < 2:
        sys.stderr.write(usage % (sys.argv[0], sys.argv[0], sys.argv[0], sys.argv[0]))
        sys.exit(1)
    
    if sys.argv[1] == 'train':
        train(sys.argv[2], sys.argv[3], 4, 3)
    elif sys.argv[1] == 'test':
        test(sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5], 10)
    elif sys.argv[1] == 'eval':
        eval(sys.argv[2], sys.argv[3])
    elif sys.argv[1] == 'html':
        formatToHtml(sys.argv[2], sys.argv[3])
    else:
        sys.stderr.write(usage % (sys.argv[0], sys.argv[0], sys.argv[0], sys.argv[0]))
        sys.exit(1)